#Atlas of Weak Signals

##Finding what is an emergent futures

I personally like the focus of our master, I think that is something powerful and unique in my formation as a designer. But also is something very new, difficult to understand and hard to explain. But through this course, it started to make sense all the knowledge that has been transmitted in the master and why the teachers are guiding us in this way. Showing the real necessity to have people prepare to design the emergent future in a proper way.

Helping us to think in a different way, not thinking in the present think in the future.

I am used to thinking like an industrial designer where you are creating products for an actual user that need to be functional and excellent business. But through this course, I change my focus in creating an interaction that is a starting point of a big topic in the future imagining a new world.

##Learning to observe

Looking the future as a moment of big changes where that changes are not starting from the air they have an evolution process and how to see them if one of the most valuable knowledge that I have learned in this course.

Developing knowledge of observations of future weak signals. Where tiny things, not the famous and official ones, the ones that pass unnoticed like; news, congress, meetings, investigations, technologies could have a big effect or opportunity in the futures.

An example of observation of future weak signals linked to my personal master project:
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/fCAX9P50SNU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
*An American studded that grows mushrooms with cigarette buts in his house.*
Show a future signal of a big solution of waste with fungi, where the mycelium is able to break down artificial material for then digested and inserted to nature.

##Getting involve in different changes

**The Future is not in 300 years more is back the corner it is in 2050.**

The future will be part of many changes. The Anthropocene epoch has made a big human impact on the Earth ecosystem that we need to fix. Actually is a big theme but in the future, they need to be ways of living without creating 0% damage to nature because is not they will die. More that a recycling system a regenerating cycle material and energy system because is not it would be hole garbage-polluted world.  

How business and job would change through the technologies that have irrupted the labor where humans are losing their works because of the irruptions inventions like robotics arms and artificial intelligence. Search for a reposition of human labor is one of the big issues.

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Ox05Bks2Q3s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*An actual good example of how robots have replaced human works: Amazon warehouse where there aren’t human interactions in almost all the process.*

##Topics that help my Master Project

My master project is about the process mycoremediation, where fungi are able to neutralize or remove contamination from waste remediating toxic pollutants. Mycelium, fungi’s root, recognizes the pollutants and produce enzymes to break down the toxins and then digest the artificial materials.

**Fight antropocen conflicts**

Realizing that the explosion of nature started time before with the industrial revolution having a slowly bigger impact to the Earth. Making in more less 100 years changes in an ecosystem that has been here a million years ago. Strengthen the necessity of human changes in how to use and interact with nature looking for regenerative solutions.

**Future of Jobs**

There could be new business coming, the jobs aren’t something stuck in the time that has usability forever. Work is also something that needs to change, replace and be improved. There are also possibilities of new business that change our manners to interact with the world.

![](assets/s_7.jpg)
*Internet mail is a good example of new work and the replacement for an old one the post office.*

##Creating the Atlas of the week signals

The power of joining different keywords creating flow charts that show the evolution of ideas and topics discovering different spaces uncovered that could be a Weak Signal.

Creating through semiotics an atlas of different information’s or links from some emergent future topics give us a toll to research, look for opportunities and see guide lines of weak signals.

##Atlas of the Week Signals Works

###Week1
Topic: circular data economy

Project: Circular Data Economy

![](assets/s_1.jpg)

*In a moment in the future where you are the owner of your data that you are producing in the cloud. So you are able to mange as you like your data information. Creating an ID card similar to the blood donator but for data donator.*

###Week2

Topic: Fighting Anthropocene conflicts

Project: The movement of spicies

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Fhhn-ikZbPA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Through observation of the movement of species because of climate change.  We create a narrative of Barcelona in 2050 invaded of malaria.*

###Week3
Topic: Future Jobs

Project: Transitioner
![](assets/s_2.jpg)

*In a moment that all the routine jobs automated and professional lifepaths are liquid because of the robotic technology. People are free to do activities with social contribution that are meaningful. We create a company that orient to match people skills and ambitions with available roles.*

###Week4
Topic: Future Material Bio

Project: Biogram
![](assets/s_3.jpg)
![](assets/s_4.jpg)
![](assets/s_5.jpg)

*Trough the observation of a weak signal of bio influencers and the idea to reused your material waste. We create a new biosocial media joined with a new type of ecologic bar code in packagings. Where you need to scan the codes and it will show you recopies of how to create useful products.*

![](assets/s_6.jpg)
*We get to this intervention through the process of mapping and link of the ideas and topics discovering a weak signal.*
