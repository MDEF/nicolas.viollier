#Design for the New

In the curse "Design for the New" guided by Mercè Rua and Markel Cormenzana from [Holon](http://www.holon.cat/en/) was helpful to imagine our project in a more real situation. Where the different practice is part of a big intervention, opening our minds to see on a big scale the influence of our project and how little changes could make practice change in different areas.

Creating a new view for designers, where we are developers of a network of practice. Actual practices like eat, clean, speak, study, walk, etc where always are here but they in the past have changed and would change also in the future. Creating a good observation of them with their associations you can create changes.

Changing the view of the project as now a big amount of practice that is known and will change in the future together. This practice has an association in different areas that are divided into 3 types; Stuff, Skills, Image.

![](assets/t3_1_1.jpg)

*The exercise of writing actual practice associated of my area of intervention and how with my project will change them in the future, exploding my mind of the approach that it can have in different actual scenarios. Helping me to realize that there are many practice areas that are associated with my project and creating a more critical view of human acts.*

Also, the practices are the join of different actors

![](assets/t3_1_2.jpg)
*Here you can see the different actors associated with my project.*


**Creating in us a more critical observation of the present and how to change it a positive way where we do not pass through human acts we work with them to improve them.**


##Prototype, Myco-Certification

My project "Regenerative Design" that work with the process of mycoremediation, where fungi are able to break down contaminants for then cleaning and neutralizing them.

Through the curse after an observation of the practice of my area of intervention and the actors associated I create the  Myco-Certification.

Imagining in the future where the mycoremediation is able to decompose almost all type of materials neutralizing waste, that is a normal thing for every city where they can bay mycoremediation capsules to break down in their home their toxins generated.

So there will be a company that cheek if the different contaminants of products are able to digest then by the fungi’s. Where like actually the industries pay to have the suitable certification know they will what to have the myco-certification that is what the customers are looking for. Paying to this company to make a test with their materials and see if the fungi digest it the standardize time without leaving any contaminants.

![](assets/t3_1_3.jpg)

*This is the certification that it will be in the products that could be remediated by fungi.*

![](assets/t3_1_4.jpg)

*This how it looks a product with his myco-certification. Where you can see the certification down the cup, so he can then throw this cup in his fungi trash can and it will discompose then.*

**Changing know the practice of how you bay a product** checking if it has myco-certification to be sure that you will be able then to break down, the packaging, the leftover or the same product after its functional use.
