#What is the Future?
Future is a mix of trends. The start point of imagining the future are trends of nowadays in worldwide use. The trends are defined as the measurement of a change of time.

![](assets/F13.jpg)
![](assets/F13_2.jpg)
*How time ago people imagine the future with trendy technology’s of that times in a common use.*

The problem is that people construct the future, not with the real image of the upcoming we imagine actual trendy things, like a very new technology, in real and global use. That it is way always when we see some futurist image or video we can understand the different factors that are showing because we know about most of them. We never have used it, because is a new trendy technology like artificial intelligence, but we know how its function.

![](assets/F11.png)

*This image of a scene of Star Wars III, a perfect example of how we imagine the future. Indicates technologies trends like robots, transhumance, and computation. We know about them and be know that they are in the actual process of devolvement’s and we never have interacted with it but in the movie, they show it in areal normal use. Our brain imagination cant rich technologies that we do not know.*

So our imagination is an extension of the present in a second tomorrow. Our brain cannot imagine a thing that goes out what we have seen or realized in our lives.

<figure class="video_container">
<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/lang/en/anab_jain_why_we_need_to_imagine_different_futures" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>
</figure>

*TED conference by Anab Jain of Superflux futuristic company. Teaching new ways of build the future.*


![](assets/F10_1.jpg)
![](assets/F10_2.jpg)
*The future are narratives, you can see a good explanation of it in the illustration of the designer Luigi Serafini in his encyclopedia Code Seraphinianus (1976-1978) of an imaginary future world explain it.*

##Future Worshops

###Future Canvas

![](assets/F15.jpeg)
*Our final canvas. With a canvas structure, we need to create a future scenario with two axis scenarios cards that we have taken.*

####Reflection

At first, I hated this exercise because I don’t like the canvas system, used for designing business plans, but after going on using it in a futuristic method I realize that in this case was very useful because it makes me think in the different variables that can happen in the future with the topics selected.

This futurist canvas was excellent because we take two axis cards with the scenario "Bizzaro Axis" and the other of "Decolonial Axis" and this method help us imagine all variables, how to join them and go to the key events from now to the future.  Making us open our imagination creating scenarios out of the common stuff without limits thinking in a future where all can be done.

Finishing, creating a world where the people were self-sufficient, they do not need to bay anything they create all that they have and need, and the people can take form special roots emotional skills, so they can take happiness or energy or whatever that you need.  So the final future scenario was named like "Self creationism bizarre roots".


###Create future scenarios workshop

![](assets/F14.jpg)
*Our final scenario result. This workshop about made a future scenario through a specific topic in where it involves all the world and make a big change. The scenario needs to generate consequences and that consequences create sub consequents. The them we choose was inequality.*

####Reflection

Was a very good method looking forward to my personal project and create a future scenario for it, open our mind in the positive and negative consequence and also what that consequent generate.

This workshop helps us see the different consequences that could have in a big scale different topics that are in nowadays present like our inequality topic.

Realizing also, that there are many technological developments in nowadays that in the future would be used globally, as the autonomous car, solving many actual problems. But there are other topics like inequality that we haven’t work so much in it and if we don’t fix it it will be worse and worse in the future with a bigger gap of separation.

###My future project scenario

![](assets/F16.jpeg)
*Working in my first personal project scenario.*

After having a week of an introduction of the future learning how to construct a possible future I took the same methods done in class and I make it with my personal project of "Hacking mushrooms to join artificial materials to the natural cycle of nature"

Creating a very powerful scene that let me see and imagine the different powerful consequence that my project could have. Creating more strong arguments that prove my area of intervention. Generation positive consequence but also negative consequences that I need to manage, like the lack of Oxygen because mushrooms breathe like humans.


##Hacking Mushrooms
![](assets/F12.jpg)
*Process of growing mushrooms.*


My new step for my personal project and are of intervention about **"Hacking mushrooms to join artificial materials to the natural cycle of nature"** was to began to experiment in it and prove it.

So I went to the laboratory of the Fab Textile trough to have the help of an expert in growing mycelium. Were they help me and together we began to train mushrooms to eat as a first project step cigarette butts of the street of Barcelona.

This was the laboratory work steps of the week:

![](assets/F1.jpg)
*1. Cultivate mycelium, to types Oster and Mk, that it will be the mothers of the ones that you will train. So I will take a little bit of mycelium of the two types and put them in two petri dish with agar.*

![](assets/F2.jpg)
![](assets/F2_1.jpg)
*2. Crete the agar substance (200ml dechlorinated water, 4gr agar agar, 4gr Malt veggie syrup)  that it would like the house of the growing mycelium, he uses it to transport, eat and move.*

![](assets/F3.jpg)
*3. Sterilized the agar substance and the new cigarette buts to take all the bacteria that could be in them to live the mycelium grown to allow without other that steal his food.*

![](assets/F4.jpg)
*4. Then prepare the space to make the experiment sterilizing the space with alcohol and put on a lighter to don’t let anything go in the samples.*

![](assets/F5.jpg)
*5. Put the sterilized agar in a new petri dish.*

![](assets/F6.jpg)
![](assets/F6_2.jpg)
*6. Put the new cigarette butts sterilized in the same last petri dish.*  

![](assets/F7.jpg)
*7. Leave them to grow in an incubator at 25˚C.*

![](assets/F8.jpg)
![](assets/F8_2.jpg)
*8. The two types of mushrooms growing with the new filter before one day.*

![](assets/F9.jpg)
![](assets/F9_2.jpg)
*9. Before three days, the two types of mushrooms growing with the new filter accepting the artificial material and beginning to eat it.*

The next week the idea is if the mushrooms are growing healthy, so they have created the enzymes to digest the plastic, is to introduce in them a used cigarette butts from the street.
