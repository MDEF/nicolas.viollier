#Finding my identity

##Exploring Hybrid Profiles


###Ariel Guersenvaig

This argentine designer make me realize haw to make design research’s, haw interesting it is and the many information that you can create and analyze. The research in design is an open gage were is all most nothing defined, witch is a excellent opportunity for us designer’s to take advantage of it and have the liberty to speculate. That is why he create a book named "What is design for?" making people thing the really function of this knowledge.  One example of design research, is he talk about something that is obvious for us the "Design Process" but he make us realize that it have planting interesting information, like prototyping is to create a dialogue out of you head placing it in the real world.
![](assets/E13.jpg)
*Ariel change of design focus.*

###Sara González

This Spain architect make me realize the value of leering the special technics of different people specialist fabricating something’s like she learn haw to manufacture shoes with the shoes craft maker of Liceu, one of the best opera theater of Barcelona, not to make the same types of shoes ore whatever but to question the fabric system, realize production problems and make design innovation about it.  So you can create really different things like new materials, new functionality systems and play with the trends.

All so I like she’s manner to work with materials and making innovation that don’t have limits in it, making me realize that now the society encloses materials in certain type of object’s like mushrooms are for eating and leather is for shoes but why don’t we create a shoes with some special mushrooms that have the same proprieties of leather. She opened me my mind in the creation of new materials and the different use of the common materials and extrapolating them with uses. Showing us that the materials can be intervened in many types mixed with other ones and opening new functionalities of it.

Sara make us realize that we all have ours own ideals, we all want a change of the world in some special way, so we need to design throw this focus weather you do make changes that preside this ideals. She is vegetarian when se learn to make shoes she realize that the manufacture system use many natural leather and have many scraps so she create shoes using leather scraps and because she hate animals dying she create new shoes made of plants like mushroom’s wood ore other type of material.
![](assets/E6.JPG)
*Scraps of leather shoe.*

![](assets/E7.JPG)
*Mushroom shoe.*

###Domestic Data Streamers

Company of design innovation creating a connection between information and people. Learning how design have infinity of things to create and areas to cover, they don’t want to make furniture like all the designers they want to make something new in design they realize that was a problem showing data so they create a company that make interaction intervention with information. Using design to communicate and influence people with this information in several ways.

I learn throw their projects because they mix the digital space with the physical space showing data that most of the time is showed in now a days in a screen or in some burring paper throw different types of interactions intervention that shows it in a real physical ways, where the user can intervene and participate making them realize the data taking advantage of showing information in real materials.

I like the manner that they use design and technologies to communicate and impact people, learning if you abstract some common material and placed in a different places, like a mirror in the middle of the hallway of the United Nations, designed with technologies in some information way you will create a peace of data communication very strong and shocking.

And finally I learn about them, haw to create a company making things that you really like, what steps you need to pass throw, the risk that you need to take, haw to won money and be sustainable in the time.

####Searching Data
![](assets/E9.JPG)
*My wallet data.*

![](assets/E10.JPG)
*A metal bottle data.*



##Design Vision/Reflect/Experience

The role of the designer for me was to create in different ways, could be digital, objects, services, acts, etc. Throw this tree articles I realize that design is a knowledge that have a processes of creation that could be implements and promoted in other types of works the innovation in them, because it has to be an essential part of all work. With these improvements they would be more prepared to solve problems and to create in their topics.

But design need to work the innovation process carefully because not always it will solve problems, this new intervention, in the other side it can open other big problems. The designer need to be conscious of the reality of the space, learn abut them and make changes that will not disturb their natural way of life **"Designing in the box".**


##How I am and how I want to be?

###My identity now and future
Here I will show a table were there are my actual design skills and the ones that I what to know to create my identity that I am searching to be.
![](assets/E1.jpg)

###Development program
Here I will show when I will learn and focus in the different design skill that I want to know, divided by the different weeks with different topics.

![](assets/E2.jpg)

###My in the future

####Goal
Adapt technologies to improve people life.

####Direction
Solving daily problems in different scenarios; industry, work, home, school, health, food, transport and culture, with new design innovations that join people in different ways with technologies. But without changing the essential of human interaction and contact with the world, **technologies need to be to help us but not to change us completely.**

####Business
Creating a charity company that shows and makes projects using the value of design and opens the people’s minds in the scopes of the new technologies, showing how they can improve people life in a consensus manner.

####What type of new technologies?
They are all the new innovative human creation were design could go throw and work with them like; digitals, chemical, biology, robotics, engineers, materials, fabrications, constructions and food.

####Examples

![](assets/E3.jpg)
*IKEA idea of democratic designed: form, function. quality, sustainability and affordable price.*

![](assets/E4.jpg)
*Cardboard Emergency Radio Designed to Save Lives in Chile.*

![](assets/E11.JPG)
*Museum Hospital San Pau, Barcelona. Visual representation of the past use of the establishment.*
