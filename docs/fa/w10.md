#Molding and Casting

In this week I wanted to create and organic shape made parametrically in Grasshopper programed. Because I want to now how are the outcome of organic design made in real for my further work for my master.

##1 Step_ Designing the Model

![](assets/10_1.jpg)

*In grasshopper program by parametric number I design this different natural shapes imitating rocks, minerals and diamonds.*

![](assets/10_2.jpg)
![](assets/10_22.jpg)

*Finally I choose to mold and cast this diamond. Because of it different faces and it conic shape.*

##2 Step_ Creating the rubber mold

First I model the box that then will printed in a 3D printer and then casted in rubber.

![](assets/10_3.jpg)

*First, I in cut in two the diamond, to create the two molds, dividing it in where there is a line of some face. So you then in the final outcome do not notice the two mold joins.*

![](assets/10_4.jpg)

*Then I make two boxes and insert in it the two half diamonds with a half line for the injection of the material and other to get out the air.*

![](assets/10_5.jpg)

Then I test the mold digitally to be sure that everything was well. The 3D printer, then the rubber mold and then checking if the final casted product was the same of the 3d model designed.

![](assets/10_6.jpg)
*Finally I 3D printed my two molds*


## 3 Step_ Cast the rubber mold

![](assets/10_7.jpg)

*Then after cleaning and taking it air I casted the 3d printed molds using a 60 hard rubber for high temperature.*

![](assets/10_8.jpg)
*Taking the bubbles of the rubber in the mold.*

![](assets/10_9.jpg)
*After waiting 3 days finally I have my two rubber mold ready to be casted.*


## 4 Step_ Cast the product


![](assets/10_10.jpg)

*After cleaning and realizing the rubber mold I tie up with plastics ties the two pieces.*

![](assets/10_12.jpg)

*I use this synthetic marble-casting product to create diamond similar of a real natural one.*


###Casting material Data Sheet

![](assets/10_30.jpg)

Reading the specification of the web page of the manufacture of the casting material that I will use I understood different specifications of it. Was very important. Because, I learn how to mix it, in what proportions, the mixing time and the set time.

Realizing that was a very fast solidification material despite that it only used water.

###Casting process

![](assets/10_11.jpg)

*Then after making the mixture of the material with a syringe I began to pour the liquid all the marble in the mold. Put having many problems in the final step because the material was getting dry.*

![](assets/10_13.jpg)

*After finish the purring y leave it dry and then opened I realize that the casting wasn’t complete because the injection hole was covered making me think that it was full.*

![](assets/10_14.jpg)
*Having and incomplete object.*

So I *make again the casting process* but in this case I will put three more marble material but in three different cups mixing it in different times.

![](assets/10_15.jpg)
*This was my set up of my second intent, with more cups, more materials, bigger syringe and taking time so I do not have drying material problems*


![](assets/10_16.jpg)
![](assets/10_17.gif)

*The casting process was better and more controlled having finally indicators that casting was full because from the air hole was coming out material.*


##The final product

Finally, my casted diamond. Made with a marble material mixed with a blue paint to create a more natural shape. Having a little problem in the end point and the union lines. But after sanding it a little bit I create a more soft shape looking real.

![](assets/10_18.jpg)

![](assets/10_23.jpg)

![](assets/10_21.jpg)

![](assets/10_20.jpg)

![](assets/10_18.gif)

![](assets/10_19.gif)

## Files

[Grasshopper parametric organic mineral/rock shape file](W9_Molding and Casting Files/Paramateric minerals.gh)

[3D Diamond file](W9_Molding and Casting Files/3D_Diamond.3dm)

[Rubber mold file](W9_Molding and Casting Files/Rubber_mold.stl)
