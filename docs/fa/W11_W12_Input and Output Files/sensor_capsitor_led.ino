#include <CapacitiveSensor.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(3, 1);

CapacitiveSensor   cs_7_6 = CapacitiveSensor(7,8);  // 10M resistor between pins 7 & 6, pin 6 is sensor pin

void setup()                    
{
   //cs_7_6.set_CS_AutocaL_Millis(0xFFFFFFFF);     // turn off autocalibrate on channel 1 - just as an example
//  mySerial.begin(9600); //sets serial port for communication
  pinMode(0, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(4, OUTPUT);
}

void loop()                    
{
  //  long start = millis();
    long total1 =  cs_7_6.capacitiveSensor(5);
   if (total1<10){
      digitalWrite(0, HIGH);
      digitalWrite(2, LOW);
      digitalWrite(4, LOW);
    }

    if (total1>20 && total1<100){
      digitalWrite(0, LOW);
      digitalWrite(2, HIGH);
      digitalWrite(4, LOW);
    }
    if (total1>80){
      digitalWrite(0, LOW);
      digitalWrite(2, LOW);
      digitalWrite(4, HIGH);
    }
//    mySerial.println(total1);                  // print sensor output 1

    delay(500);                             // arbitrary delay to limit data to serial port 
}
