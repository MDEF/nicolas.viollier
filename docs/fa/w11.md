#Input Devices

This assignment I made it together with **[Thomas Barnes](https://mdef.gitlab.io/thomas.barnes/fabacademy).**

![](assets/11_38.jpg)

The idea was to cover the assignment for inputs and the assignment for output in the same PCB.

##Idea

The idea was to create a **Capacitor sensor** able to create an input from the proximity of human hand. Where someone will bring his hand closer to the capacitor sensor generating different data from the proximity to the sensor.

![](assets/11_2.jpg)
*How the capacitor sensor works.*

Taking as reference many daily objects that have capacitor sensors:

![](assets/11_1.jpg)
*Soup devices of public bathrooms.*

![](assets/11_3.jpg)
*The actual touch screens of smartphones.*

##Making the Sensor
We learn that we could use many materials to create a proximity capacitor sensor could be a pencil, aluminum paper or copper. So we take the approach that we had in the Fab Lab a special copper that could be cut in the vinyl cutter and we design to use it because with this material we will be able to create a special design of the sensor and then cut it in the vinyl cutter.

![](assets/11_5.jpg)
*This is the copper paper that we will vinyl cut.*

![](assets/11_6.jpg)
*So we download a hand design form Nou project and then in Illustrator we edited a little bit having finally what you see in the picture. But when tray too cut it, it was too wide to be cut.*

![](assets/11_4.jpg)
*So then in Rhino we change the thumb having finally this.*

![](assets/11_7.jpg)
*This is the final file that will be cut, is left hand because Tom and I are left hand and we hate that everything is made for right-handed.*

![](assets/11_8.jpg)
*This was how it looks finally the hand cut in a copper paper that then we paste in an acrylic board that we laser cut before for it. Above there is a hole to insert the cable that comes from the PCB.*

##Designing the PCB

![](assets/11_9.jpg)
*Guided by Neil boards, specially Hello.load.45.*

![](assets/11_10.jpg)
*We design in Eagle our board that used an Attiny44 programmer and had the connections for the capacitive proximity sensor input but also to make two assignments’ in one we add an output that will be an RGB light that will then interact with the proximity sensor.*

![](assets/11_11.jpg)
![](assets/11_12.jpg)
*Finally we had our trace an outline ready to be a mill.*

##Milling process
The milling we think that will be something easy because we had the experience using this machine in some assignments before. But we had many issues.

![](assets/11_13.jpg)
*Here you can start seeing the first milling that we made.*

The first one when we start cutting the outline it starts moving and I stop the machine and cut it by hand the outline. But also it comes with some overlap cables so I tried to fix it and I cut another line so was a big disaster.

Then we mill again the PCB having the same problem that when we made the outline it moves so we cut it by hand and then Tom more carefully cut the overlap lines.

##Soldering and Burning

We sold the different components guided by the trace made in Eagle
and then we cheek the connections realizing that were some wrongs ones and we fixed them

But when we connected to Arduino we couldn’t burn it.

![](assets/11_15.jpg)
![](assets/11_14.jpg)
Then the teachers recommend me to add a Capacitor of 1uf from GND to VCC and a Resistor 10K from Reset to VCC, but also I do not work.

![](assets/11_16.jpg)
*I was desperate checking every connection like for a whole afternoon.*


Realizing then the problem, the MISO and the SCK were switched.

![](assets/11_18.jpg)
*This is how they were before not letting us burn.*  

![](assets/11_17.jpg)
*This is the correct position, that then I fixed in Eagle and in the PCB.*

![](assets/11_19.jpg)
Burning finally my PCB, so know I will be able to play with my input and output.


##Output
First, I try to make the Input and Output separately and I start with the output

![](assets/11_20.jpg)
So I realize that my RGB doesn’t work because I had soldered without looking the data shit of the output and I soldered in the incorrect position. So I take it and soldered know to look the data sheet in the correct position.

![](assets/11_21.jpg)
Having then a good outcome, the light turns on.

![](assets/11_23.jpg)
![](assets/11_22.gif)
The finally I charge a code that makes the RGB blink at different times the different colors.

##Input
The sensor proximity capacitor was more difficult to control because it needs much more work in the codes and I am not an expert in coding.

First I try with many opens source codes of different capacitors, made to use with an Arduino board, but I couldn’t get any date form them.

![](assets/11_28.jpg)
*Then, I realize that I need to use a library of the Arduino. That will help my board think and process with algorisms the capacitive sensor data.*

![](assets/11_24.jpg)
![](assets/11_25.jpg)
![](assets/11_26.jpg)
*So I add the "SoftwareSerial" library and add some codes and it starts incredibly to send data from the capacitor.*

But the data wasn't really changed too much when I put my hand in the sensor or if I also touch the connection from the PCB that supposedly should change drastically.

![](assets/11_27.jpg)
But then I add a new library the "CapacitiveSensor" and know it stars to read correctly the data from the proximity sensor capacitor where you can see data change from 10 when you don’t have your hand near to 300 when you have your hand over the copper hand.

##Final Interaction INPUT-OUTPUT

Then the challenge was now to join the Input mad with the Output make them interact together.

![](assets/11_29.jpg)
*So first I mix both codes.*

![](assets/11_31.jpg)

*But how you can see in this picture in do not work because the capacity of the Attinity 44 was less that Sketch.*

![](assets/11_30.jpg)

So I finally comment on the "serial read" steps so the board will read the data for the capacitor but not need to show it so he will need to think less. Finally making the board read the code.*

![](assets/11_34.gif)

And then, start incredibly to interact the Input with the Output, where you can see that from the near you were of the sensor the light of the RGB began to change.

![](assets/11_33.gif)

The data wasn’t totally controlled, I need to adjust the values.

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Vqy69FqWOsk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Here you can see then when the data and the output is more controlled.*

![](assets/11_35.gif)
![](assets/11_36.gif)

*Also I adjusts the colors that it will show in the different interactions by changing the output pins*

![](assets/11_32.jpg)

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/jwuxoWzx2RQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

![](assets/11_37.gif)

*Finally creating a more perfect interaction, after adjusting the values of the capacitor to create a more defined outcome, where the hand when is far is Red, when is near is blue and when is touching it is purple. Adding also an acrylic piece to see a better light.*  

## Files

[Arduino Code](W11_W12_Input and Output Files/sensor_capsitor_led.ino)

[Vinyl Cut Hand](W11_W12_Input and Output Files/hand.ai)

[Eagle PCB Design File BRD](W11_W12_Input and Output Files/PCB Drawing.brd)

[Eagle PCB Design File SCH](W11_W12_Input and Output Files/PCB Drawing.sch)

[Outline image](W11_W12_Input and Output Files/PCBoutline.png)

[Trace image](W11_W12_Input and Output Files/PCBtrace.png)
