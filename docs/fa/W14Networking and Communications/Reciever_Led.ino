#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>
 
const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);
 
// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 4747;
 
char data[4];
 
void setup(void)
{

   pinMode(6, OUTPUT);
   
   Serial.begin(9600);
   radio.begin();
   radio.openReadingPipe(1,pipe);
   radio.startListening();
}
 
void loop(void)
{
   if (radio.available())
   {
      radio.read(data, sizeof data); 
      Serial.println(data);

      int c = (int)data;

         if (c > 200) {digitalWrite(6, HIGH);
          }
   }
   
}
