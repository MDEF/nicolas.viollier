![](assets/FP_2_2.jpg)

The Fungi Kingdom is one of the biggest and more important decomposers in nature. Breaking down natural materials into simple organic forms that are a food source for many other species.

Fungi are able to discompose almost everything. Through the process mycoremediation, they are able to neutralize or remove contamination from waste remediating toxic pollutants.

Mycelium is the fungi’s root system, which recognizes pollutants and produces enzymes to break down the toxins to digest the artificial materials. Transforming these artificial materials into organic waste integrating it to nature.

I went through the topic of mycoremediation as a solution to one of the biggest issues in our time. Waste and the contamination of our planet. Looking for a new way to solve it with a regeneration system of materials, where everything is part of a natural cycle. I was not thinking to create recyclable products that most of the times aren’t a real solution, and create in the process more pollution even if they are processed and used again.  

![](assets/FP_2_8.jpg)

My starting experiment was with cigarette butts, one of the most commonly littered waste product in the world. More than 4,5 trillion are littered each year in the world.

I created a training system of Oyster mushrooms to breakdown and digest cigarette butts from the streets of Barcelona. Achieving finally mycelium that digests the artificial material transforming it into organic material.

![](assets/FP_2_11.jpg)
*Mycelium starting to break down a street cigarette but.*

After 3 months of incubation, a mushroom finally grew.

![](assets/FP_2_12.jpg)
*Mushroom growing from the digestion of cigarette butts.*

We need species that help us spread these bioremediation fungi.
Remediating toxic pollutants from waste.

Do you know that humans have more than 200 toxins in our body? When we die we are buried in cemeteries contaminating nature even more with our body.

![](assets/FP_2_7.jpg)
*Data of human pollution.*


**Why not growing mushrooms with human bodies?**

Remediating the toxins that humans have, and then spreading species of mycelium, using mycoremediation to tackle pollution in nature.

**Transforming the significance of human death.**

Where your life does not finish when you die after you are transformed into a mushroom that is spread into nature neutralizing contamination from waste remediating toxic pollutants. Repairing the future for the rest.

**How to grow mushrooms with a human body?**

First, you need to train the mushroom before you die with human samples like nails or hair, so it learns about the material and creates the correct enzyme to break down the material. After the mushroom is trained in creating the correct enzyme, he would be able to digest your toxic body when you die.

![](assets/FP_2_13.jpg)
*Training Oyster Mycelium with my beard for then one I day he can digest my body and then help the remediation of toxins.*

![](assets/FP_2_6.jpg)

Through this mushroom training system, I create: "Growing my Legacy Incubator"

![](assets/FP_2_23.jpg)
*Renders of the "Growing my Legacy Incubator".*

![](assets/FP_2_14.jpg)
![](assets/FP_2_15.jpg)
![](assets/FP_2_16.jpg)

*Prototype presented of the incubator.*

![](assets/FP_2_9.jpg)
*Explanation showed in the presentation of the functionalities of the incubator.*

A fungi incubator that helps you grow your future after death. Where you can feed, grow and see what you will be in the future.

**Changing the way to go through death.**

Generating a personal way to be prepared for death being conscious of it but looking at it in a positive way as something normal and not like the final stage of your life. A new step helping the rest of living humans.

Cemeteries will no longer be a sad place, they will be now a place of connection between nature and our ancestors. Seeing people transformed by another species of life, and how they are helping us to repair the world.


##Mapping

How it would be if we change all the cemeteries of Barcelona for fungi mycoremediation parks?

![](assets/FP_2_4.jpg)
*Timeline of the toxic remediation.*

![](assets/FP_2_5.jpg)
*The growth of the mycelium.*

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/wJ3YrXjFrx0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
*This would be the growth of mycelium remediating toxins throughout the city.*

##Pictures of the presentation

![](assets/FP_2_17.jpg)![](assets/FP_2_18.jpg)![](assets/FP_2_19.jpg)![](assets/FP_2_20.jpg)![](assets/FP_2_21.jpg)![](assets/FP_2_22.jpg)

## Material Presented

![](assets/FP_2_3.jpg)

*Experimentation laboratory process.*

![](assets/FP_2_1.jpg)

*Taking the idea out of the lab more in the house.*

##Reflection

**Focus in the remediation system**

I will focus more on creating a second life with the mycoremediation system that is the powerful opportunity of my project, making a life of something that is dead. But not focusing on human death that brig to the project much bigger social issues that distract the main focus of the project.

**Amplify the project**

I will go in the direction of creating a biosystem that helps to remediate pollution in different areas. Designing an incubator that help industry, restaurants, shops, houses, etc to clean the nature and regenerate material form waste.

**The power is in the process not in the product**

The incubator or whatever is a result and a materialization but the innovation and the interaction are in the process of training mycelium to digest artificial materials and solve the problem of waste.
**Make many test with different food**
I need to experiment with different types of waste and toxins and see what happen. Control the time, measure the effects and then show the process and differences.

**What are 219 toxic? communicate visually.**

I need to show in a graphic design manner the different data that I get. Like if the human body has 219 toxic say that is the some of 1000 plastic bottles in the ground. Making the information easy for the audience that will prove better my idea.

**Check the remediation system**
Verify with scientific data the mycoremediation system of mycelium is something very important is the starting point of my project.

**Death afterlife is the powerful engaging idea but not the hall project**

I need to use the idea of growing a second life with humans like a manner to attract attention and show the power that can be my project but is not my whole project. My project is about growing a second life with everything not only humans, could be a cigarette but, a packaging, oil, etc.

**Lock forward a regenerative concept**

I am searching for a future manner to solve the problem of pollution looking at the future. Where I think that the real solution is the regeneration of material, not recycling. The actual system has issues because many times produce more contaminations in the process more than it could produce if you threw it in a landfill.

**Why not this mycoremediation system is a bracelet or a teddy bear that take all your neutral material?**

The incubator still looks to medical I need to make it more housie. I need to observe and think about how to create this death growing system like something normal that you will be touching or carrying every day without noticing that is taking your neutral waste.

##Next steps

1. Prove the bio neutralization of toxins. 1 week.

2. Research, buy and grow new types of fungi. 2 weeks.

3. Search and contact partner’s in the mycoremediation systems. 2 week.

2. Test with different materials and fungi’s. 3 week.

3. Research about if it transforming material or cleaning. 3 week.

4. Create the training incubator with whole step process of remediation.  4 and 5 weeks.

5. Create a graphic design that shows with data and in an easy way what is happing in the mycelium mycoremediation process. 6 week.
