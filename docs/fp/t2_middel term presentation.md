#Middle Term Review

During the process of a project you many times get in different topics looking for the same idea. That makes it powerful, with more arguments and scalable to different areas. Without the problem to be so specific that it will only cover a little issue.

So in this part of the master, I was in a direction a little bit different from the fungi world but with the same focus and searching to the same are of intervention.

![](assets/MT-01.jpg)
*First slide, presenting my general idea go through.*

##New material intervention

I was discovering new materials in the subject material driven design. Where my idea was to create a material that can make an intervention and statement of how wrong we are using materials, and how powerful are many things that we are littering every day.

We are currently in a big problem of waste, filling the earth with trash that is destroying nature. These problems start in the manufacturing system where they are creating products with materials that aren’t made and thought to be part of a circular cycle like nature does. A leaf falls from a tree, then it is digested by bacteria and it is converted into the soil to grow again into a tree. We are producing in a linear cycle named by William McDonough from the cradle to the grave.

I searched for biomass that can cover this statement and found bones. A material that is thrown away every day in butchers without taking advantage of its function, as a structural material.

![](assets/MT-05.jpg)
*One of my slides, presenting the properties of bones.*

Where I finally created an inorganic biomaterial cement made of pure calcium powder. It was made from bovine bones obtained for free from butchers mixed with a little bit of bone glue that works like a binder. It makes a strong and light cement that can be used for construction and comes from completely the same source.

![](assets/MT-06_.jpg)
*My bioceramic cement made of cow bones.*

##Presentation

So then for my personal project after obtaining these excellent outcomes creating a bone cement I though how to create an intervention with this biomass that will show how we should manufacture in a circular natural cycle.

Then I discover that bones calcium powder is rich in phosphorus mineral because of bones area composite material compound of 70% inorganic mineral materials and 30% organic.  Phosphorus has very important functionalities in plants. They need it for important growing roles like; photosynthesis, root development, respiratory, energy storage, and production cycle.

![](assets/MT-06.jpg)
*My 6 slide, showing the importance of phosphorus in plants.*

Actually, the agriculture industry is creating a big problem because instead of using natural fertilizers, like bone powder, for the crop they are using artificial ones. Growing quicker and cheaper with artificial fertilizer that imitates natural mineral’s. Creating after big problems in our health eating many time fruits and vegetables that have toxic chemicals.

![](assets/MT-03.jpg)
*The agriculture system.*

**Why we are throwing the bones and not using it to grow healthy plants?**

After learning the functionalities of calcium for growing plants that shows how different entities of nature like a plant and a cow, that many times in nowadays they separated so much, are both parts of the natural cycle. So I thought about the way not to create a calcium cycle?

![](assets/MT-07.jpg)

*At first I have this cycle where the bones of cows are used to grow grass that then the same cow will eat closing a circular manufacture process.*

###An Intervention that makes you react

The cycle was too simple and don’t make move you to change our manufacture productivity system. So then I thought:

**Actually is healthier to grow crops with human bones?**

Human bones are full of phosphor’s minerals that will grow healthy plants and we are wasting this powerful fertilizer cemetery. Why don’t we used them instead of growing plants with artificial materials that the only thing that they are doing is damaging our bodies.

![](assets/MT-08.jpg)
*Intervention cycle.*

In a manner to see and test in the presentation the reaction of the people about changing the agricultural system in the future growth with human bones because all the damage that we have to the soil with chemicals how bad nutrient it used to grow them. That the only manner to remediate it is to begin fertilizing crops with human bones.  I create this prototype intervention:

![](assets/MT-09.jpg)
*On the left shows our actual reality of how disgusting the agriculture industry grows, what we eat, in an exaggerated way presenting the artificial fertilizer like pieces of plastics and cigarette butts. Growing a sad and unhealthy plant. On the other side how healthy and natural would be if we cultivated with human bone as a fertilizer.*

Making the question "Which one will you chose?". To make the people think and struggle in the decision. Creating in them a reaction to change the actual production system.


##Reflections and Comments

**Make statements**
Start to make a decision of what you are doing looking for a position and direction.

**Human Bones**
Using it as a way of paying for your bad used of the planet. Making people uncomfortable.

**Connect introduction with conclusion**
Too much introduction and light conclusion. Create a type of connection and process that connect where you start and where I finish.

**Show data**
To prove what are you saying used real data that make a base of your area.

**Cultural issue**
If I want to go with the topic of death I will need to fight with big cultural issues.

**Finally is fertilizer**
In an easy way what I am creating is a natural fertilizer with a big story beneath.

**Good start with Mycoremediation**
How I start my project was a really interesting topic sadly that was going through it.
