#My final presentation

To show my project I use a method of making a mixing printed things and my real experiment, like a physical infographic. Creating an exhibition that plays with paper and sciences experiment’s results in Petri dishes. Showing the data, result and the research in a printed planning way that join the experiment result and make it all understandable without the presence of someone explaining it.

## Material presented

My presentation consists in two infographic boards, one shows the research and the learning from the class, the laboratory process of training mushrooms and the experiment results, and in the other board the future scenario of my project and the next experiment test.   

This are the details of my exhibition.

![](assets/P1.jpeg)
*This is the hole staging of my presentation, with the two boards, the Petri dishes and above, the name of my project, my vision, the how and the start point.*

![](assets/P2.jpeg)
*Here you can see ant left side the Petri dishes with the experiment step process where a show how I train mushrooms to eat street cigarette buts and at the right the experiment result of growing mushrooms*

![](assets/P3.jpeg)
*This is the most important final result experiment, where I show a mycelium growing after he have eaten completely a cigarette butt from the street.*

![](assets/P4.jpeg)
*I join the research with the step by step of the process of training a mushroom.*

![](assets/P5.jpeg)
*The other side of the presentation showing a flowchart of the future scenario and down the next material that I what to make the mushroom digests.*

![](assets/P-03.jpg)
*This are the objective and the view of my final presentation.*

![](assets/P-01.jpg)
![](assets/P-02.jpg)
*This is the infographic of the two boards, in where the black circles were for the petri dish using that color to highlight what was in them.*





##Feedback of improving your project

The different comments and feedback of the teachers and jury were very helpful to see in other eyes experiences and form expert people. Making my think in the next process of my project with a better objective.  


###Looking for an specify objective
On one side the biology feedback that I have was that I can go throw my project in 3 types of ways:

1. Something very heard in nowadays that is to create biomaterials and all type of ecologic polymers that are able to be into the natural cycle.

2. Other is to create an ecosystem that with mushrooms, bacteria’s and other factors, we can create like and ambient that helps this digest to eat in a proper way the trash and then create some useful product like soil or food.

3. Or the one that I think that is more powerful is to change the DNA of mushrooms and create by ultraviolet and selection new species of a mushroom eater of trash and growing healthy mushrooms. Making a genital change and create new species. Think that my experiment result now isn’t healthy mushroom’s.

Other very useful feedback is that I am now betting for too much, training to make nature eat all the trash, is something too big, trash is something too wide and nature also to enormous. I need to focus on something like mushroom’s that eat some type of trash or could no to be to create healthy mushroom that could help to create a special soil, etc.

Thinking in one objective using different research and paper for other places because you could not create all.

###The process is something powerful

Also, another way to thin in my final product or project is to appreciate it process that if have done of training the mushroom. So why not to create an educative kit that teaches you about mushrooms and how to make them digest trash.

###Experiment

Play with all types of materials and make experiments, a then conclusion of all and finally pick the most valuable result and show them.

I need to show more data sciences of my result no only showing the image result like that if have eating the filter for example. I need to prove with real data way I am saying that answered.  Like the scientist’s do they don’t show the image they show the numbers or description that demonstrate what he is saying.

###Join a corporation

The are many corporations of recycler or recollection of specific trash like the charity corporation that collect cigarette butts in Barcelona beaches. Join them and see what they do after with the material. They could be my next partners.  

![](assets/P6.png)
*Facebook group of cigarette butts of the beach recollection.*


###Create Citizen Scenario

Improving my future scenario think I need to go out of the laboratory and think in more personal and citizens use of it in the future. Think in an urban scenario with this new vision of the future. How people would change? How their action would change? To they eat in a different way? Were this system would be in the city, do they have in their house or in the neighborhood or in more government and municipality access?
